package utils;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static int addColors(int color1, int color2) {
        if (color1 == color2) {
            return color1;
        }
        int newId = 10 * (color1 + color2);
        List<Integer> integer = new ArrayList<>();
        for(GameColors gameColors : GameColors.values()){
            integer.add(gameColors.getId());
        }
        System.out.println(newId + " " + color1 + " " + color2);
        if(!integer.contains(newId)){
           return 510;
        }
        return newId;
    }


}
