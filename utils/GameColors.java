package utils;

import javafx.scene.paint.Color;

public enum GameColors {
    RED(1, Color.RED),
    BLUE(2, Color.BLUE),
    GREEN(3, Color.GREEN),
    MAGENTA(30, Color.MAGENTA),
    YELLOW(40, Color.YELLOW),
    CYAN(50, Color.CYAN),
    PINK_VIF(310, Color.PINK),
    ORANGE(410, Color.ORANGE),
    BLACK(510, Color.BLACK),
    BLACK2(330, Color.BLACK),
    LIME(430, Color.LIME),
    MENTHE(530, new Color(0.0862745f, 0.721568f, 0.305882352f, 1f)),
    PURPLE(320, Color.PURPLE),
    BLACK3(420, Color.BLACK),
    BLEUROI(520, Color.ROYALBLUE);


    private int id;
    private Color color;

    private GameColors(int id, Color color) {
        this.id = id;
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public int getId() {
        return id;
    }

    public static GameColors getColor(int id) {
        GameColors reG = null;
        for (GameColors gameColors : GameColors.values()) {
            if (gameColors.getId() == id) {
                reG = gameColors;
                break;
            }
        }
        return reG;
    }

}
