package sample;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class MainScreen {
    public static Stage fg_pregame_stage;
    File file = new File("libs/PlaneteCookie21.png");
    File file1 = new File("libs/etoiles.png");
    File file2 = new File("libs/etoiles_hover.png");
    File file3 = new File("libs/PlaneteNorm1.png");
    String s = "✘";
    String x = "✔";
    Image image = new Image(file1.toURI().toString());
    Image hoverimage = new Image(file2.toURI().toString());
    ImageView view = new ImageView(image);
    ImageView view1 = new ImageView(image);
    ImageView view2 = new ImageView(image);
    ImageView view3 = new ImageView(image);
    BackgroundImage BI = new BackgroundImage(new Image(file.toURI().toString()), BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
    BackgroundImage BI2 = new BackgroundImage(new Image(file3.toURI().toString()), BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
    @FXML

    private AnchorPane anchor;

    @FXML
    public void initialize() {
        if (!Main.premièreEpreuve || !Main.deuxiemeEpreuve || !Main.troisièmeEpreuve) {
            anchor.setBackground(new Background(BI));
            anchor.getChildren().add(view);
            view.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    /*TODO : LANCER UNE POPUP EXPLICATIVE

                     */
                    if (MouseButton.PRIMARY.compareTo(event.getButton()) == 0) {
                        if (!Main.premièreEpreuve) {
                            new JOptionPane("").showMessageDialog(null, "<html><body>" + "Première Epreuve : Le SMECTA -> " + "<font color=\"RED\"><span>" + s + "</span></font>"
                                    + "<br>Oh non ! Le Grand Méchant Cookus a rendu malade le volcan !"
                                    + "<br>A cause de cela il pète ! Il a rendu sale la planète !"
                                    + "<br>Elle est en forme de parallélogramme et l'eau est rouge !"
                                    + "<br>Il faudra fabriquer du SMECTA pour volcan !"
                                    + "<br>Voici la liste des ingrédients :"
                                    + "<br>- Acide pikachuïdal"
                                    + "<br>- SpriteSaMere-anhydre"
                                    + "<br>- LigueOfLinalol"
                                    + "<br><br>Si tu es près Ok -> Boutton droit"
                                    + "</body></html>");

                        } else {
                            new JOptionPane("").showMessageDialog(null, "<html><body>" + "Première Epreuve : Le SMECTA -> " + "<font color=\"GREEN\"><span>" + x + "</span></font>"
                                    + "<br>Oh non ! Le Grand Méchant Cookus a rendu malade le volcan !"
                                    + "<br>A cause de cela il pète ! Il a rendu sale la planète !"
                                    + "<br>Elle est en forme de parallélogramme et l'eau est rouge !"
                                    + "<br>Il faudra fabriquer du SMECTA pour volcan !"
                                    + "<br>Voici la liste des ingrédients :"
                                    + "<br>- Acide pikachuïdal"
                                    + "<br>- SpriteSaMere-anhydre"
                                    + "<br>- LigueOfLinalol"
                                    + "<br><br> Mais tu as réussi ! T'es fort nan ? Fait le prochain défi !"
                                    + "</body></html>");
                        }
                    } else {
                        Stage stage = new Stage();

                        try {
                            Parent root = FXMLLoader.load(getClass().getResource("fg_pregame.fxml"));

                            stage.setTitle("1er Jeu - PreGame");
                            Scene scene = new Scene(root, Main.width, Main.height);
                            scene.setOnKeyPressed(event1 -> {
                                if (event1.getCode().equals(KeyCode.ESCAPE)) {
                                    stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
                                }
                            });
                            stage.setScene(scene);
                            stage.initStyle(StageStyle.TRANSPARENT);
                            stage.setResizable(false);
                            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                                @Override
                                public void handle(WindowEvent event) {
                                    stage.hide();
                                    Controller.stage.show();
                                }
                            });
                            fg_pregame_stage = stage;
                            stage.show();
                            Controller.stage.hide();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            view.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    view.setImage(hoverimage);
                }
            });
            view.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    view.setImage(image);
                }
            });
            view.setLayoutX(405);
            view.setLayoutY(395);


            anchor.getChildren().add(view1);
            view1.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    new JOptionPane("").showMessageDialog(null, "<html><body>" + " Crédits "
                            + "<br><br> <span  style=\"font-weight: bold; color:blue; text-decoration: underline;\">Equipe : </span>"
                            + " <br><span  style=\"font-weight: bold;\">Scénariste en chef : </span>"
                            + "  <br>-- Mélina Montei"
                            + " <br><br> <span  style=\"font-weight: bold;\">Scénaristes : </span>"
                            + "  <br>-- Isa Silva Santos"
                            + "  <br>-- Louis Yassaï"
                            + "  <br>-- Colin Thomas"
                            + " <br><br> <span  style=\"font-weight: bold;\">Doubleur :</span>"
                            + " <br>-- Louis Yassaï"
                            + " <br><br> <span  style=\"font-weight: bold;\">Développeur : </span>"
                            + "  <br>-- Colin Thomas"
                            + "<br><br> <span  style=\"font-weight: bold; color:blue; text-decoration: underline;\">Remerciement : </span>"
                            + "<br>-- Etienne Thomas - Graphisme ( dessins )"
                            + "<br><br><br> <span  style=\"font-weight: bold;\"> -- Cookus The Cookie par M. Montei, L. Yassaï, I. Silva Santos, C. Thomas</span>"
                            + "</body></html>");
                }
            });
            view1.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    view1.setImage(hoverimage);
                }
            });
            view1.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    view1.setImage(image);
                }
            });
            view1.setLayoutX(1179);
            view1.setLayoutY(550);

            anchor.getChildren().add(view2);
            view2.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                }
            });
            view2.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    view2.setImage(hoverimage);
                }
            });
            view2.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    view2.setImage(image);
                }
            });
            view2.setLayoutX(592);
            view2.setLayoutY(224);

            anchor.getChildren().add(view3);
            view3.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                }
            });
            view3.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    view3.setImage(hoverimage);
                }
            });
            view3.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    view3.setImage(image);
                }
            });
            view3.setLayoutX(187);
            view3.setLayoutY(113);
        } else {
            anchor.setBackground(new Background(BI2));
            anchor.getChildren().add(view1);
            view1.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    new JOptionPane("").showMessageDialog(null, "<html><body>" + " Crédits "
                            + "<br><br> <span  style=\"font-weight: bold; color:blue; text-decoration: underline;\">Equipe : </span>"
                            + " <br><span  style=\"font-weight: bold;\">Scénariste en chef : </span>"
                            + "  <br>-- Mélina Montei"
                            + " <br><br> <span  style=\"font-weight: bold;\">Scénaristes : </span>"
                            + "  <br>-- Isa Silva Santos"
                            + "  <br>-- Louis Yassaï"
                            + "  <br>-- Colin Thomas"
                            + " <br><br> <span  style=\"font-weight: bold;\">Doubleur :</span>"
                            + " <br>-- Louis Yassaï"
                            + " <br><br> <span  style=\"font-weight: bold;\">Développeur : </span>"
                            + "  <br>-- Colin Thomas"
                            + "<br><br> <span  style=\"font-weight: bold; color:blue; text-decoration: underline;\">Remerciement : </span>"
                            + "<br>-- Etienne Thomas - Graphisme ( dessins )"
                            + "<br><br><br> <span  style=\"font-weight: bold;\"> -- Cookus The Cookie par M. Montei, L. Yassaï, I. Silva Santos, C. Thomas</span>"
                            + "</body></html>");
                }
            });
            view1.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    view1.setImage(hoverimage);
                }
            });
            view1.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    view1.setImage(image);
                }
            });
            view1.setLayoutX(640);
            view1.setLayoutY(360);
        }


    }
}
