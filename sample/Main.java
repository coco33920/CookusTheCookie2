package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

public class Main extends Application {
    public static Stage primaryStage;
    public static boolean premièreEpreuve = false;
    public static boolean deuxiemeEpreuve = false;
    public static boolean troisièmeEpreuve = false;
    public static Screen screen = Screen.getPrimary();
    public static double width = screen.getVisualBounds().getWidth();
    public static double height = screen.getVisualBounds().getHeight();

    @Override
    public void start(Stage primaryStage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hello World");
        Scene scene = new Scene(root, width, height);
        scene.setOnKeyPressed(event1 -> {
            if (event1.getCode().equals(KeyCode.ESCAPE)) {
                primaryStage.fireEvent(new WindowEvent(primaryStage, WindowEvent.WINDOW_CLOSE_REQUEST));
            }
        });

        primaryStage.setScene(scene);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setResizable(false);
        primaryStage.show();
        this.primaryStage = primaryStage;
    }


    public static void main(String[] args) {
        launch(args);
    }
}
