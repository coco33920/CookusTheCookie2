package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.input.KeyCode;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class FgPregrame {

    @FXML
    private WebView web;

    @FXML
    private Button b;


    @FXML
    public void initialize() {
        WebEngine engine = web.getEngine();
        File f = new File("libs/www/explication.html");
        engine.load(f.toURI().toString());
        b.setOnAction(event -> {
            try {
                Parent root = FXMLLoader.load(getClass().getResource("fg_game.fxml"));
                Scene scene = new Scene(root, Main.width, Main.height);
                Stage stage = new Stage();
                stage.initStyle(StageStyle.TRANSPARENT);
                scene.setOnKeyPressed(event1 -> {
                    if (event1.getCode().equals(KeyCode.ESCAPE)) {
                        stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
                    }
                });
                stage.setScene(scene);
                stage.setResizable(false);
                stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                    @Override
                    public void handle(WindowEvent event) {
                        MainScreen.fg_pregame_stage.show();
                    }
                });
                MainScreen.fg_pregame_stage.hide();
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }


}
