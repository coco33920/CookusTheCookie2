package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;

public class Controller {

    public static Stage stage;

    @FXML
    private MediaView view;

    @FXML
    private ProgressBar progress;

    public static MediaPlayer mediaPlayer;
    public static Media media;

    @FXML
    public void initialize() throws InterruptedException {
        view.setFitHeight(Main.height);
        view.setFitWidth(Main.width);
        File file = null;
        file = new File("libs/CCC2.mp4");
        media = new Media(file.toURI().toString());
        mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setStartTime(Duration.ZERO);
        view.setMediaPlayer(mediaPlayer);
        view.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mediaPlayer.stop();
                mediaPlayer.stop();
                Stage stage = new Stage();

                try {
                    Parent root = FXMLLoader.load(getClass().getResource("mainscreen.fxml"));

                    stage.setTitle("Acceuil");
                    Scene scene = new Scene(root, 1280, 720);
                    scene.setOnKeyPressed(event1 -> {
                        if (event1.getCode().equals(KeyCode.ESCAPE)) {
                            stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
                        }
                    });
                    stage.setScene(scene);
                    stage.initStyle(StageStyle.TRANSPARENT);
                    stage.setResizable(false);
                    stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                        @Override
                        public void handle(WindowEvent event) {
                            Main.primaryStage.hide();
                        }
                    });
                    stage.show();
                    Controller.stage = stage;
                    Main.primaryStage.hide();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        mediaPlayer.setAutoPlay(true);
        mediaPlayer.setOnEndOfMedia(new Runnable() {
            @Override
            public void run() {
                System.out.println("CS");
                mediaPlayer.stop();
                Stage stage = new Stage();

                try {
                    Parent root = FXMLLoader.load(getClass().getResource("mainscreen.fxml"));

                    stage.setTitle("Acceuil");
                    Scene scene = new Scene(root, 1280, 720);
                    scene.setOnKeyPressed(event1 -> {
                        if (event1.getCode().equals(KeyCode.ESCAPE)) {
                            stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
                        }
                    });
                    stage.setScene(scene);
                    stage.initStyle(StageStyle.TRANSPARENT);
                    stage.setResizable(false);
                    stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                        @Override
                        public void handle(WindowEvent event) {
                            Main.primaryStage.hide();
                        }
                    });
                    stage.show();
                    Controller.stage = stage;
                    Main.primaryStage.hide();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        });

    }


}
