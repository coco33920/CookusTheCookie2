package sample;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import utils.GameColors;
import utils.Utils;

import javax.swing.*;
import java.util.*;
import java.util.stream.Collectors;

public class FgGame {


    @FXML
    private AnchorPane pane;


    private HashMap<Button, HashMap<Integer, Integer>> buttons = new HashMap<>();
    public static double width = Main.width;
    public static double height = Main.height;
    private double lignes = 0;
    private double colonnes = 0;
    private Background whiteBackground = new Background(new BackgroundFill(Color.WHITE, null, null));
    private Background blackBackground = new Background(new BackgroundFill(Color.BLACK, null, null));
    private Border redBorder = new Border(new BorderStroke(Color.DARKRED, BorderStrokeStyle.SOLID, null, null));
    private double cases = lignes * colonnes;
    private boolean inE = false;
    private int iEiD = -1;
    private Button inEB;

    private boolean PIKACHUIDAL_ACID = false;
    private boolean LEAGUE_OF_LINALOL = false;
    private boolean SPRITESAMERE_ANHYDRE = false;

    private boolean PIKACHUIDAL_ACID_ONE = false;
    private boolean LEAGUE_OF_LINALOL_ONE = false;
    private boolean SPRITESAMERE_ANHYDRE_ONE = false;

    int ie = 0;


    @FXML

    public void initialize() {
        double ratio = width / height;
        String frac = convertDecimalToFraction(ratio);
        String[] f = frac.split("/");
        lignes = Double.parseDouble(f[1]);
        colonnes = Double.parseDouble(f[0]);
        y();
        colorCases();
    }


    public void y() {
        double base_x = Math.round(width / colonnes);
        double base_y = Math.round(height / lignes);
        double cx = 0;
        double cy = 0;

        for (int i = 0; i < lignes; i++) {
            for (int o = 0; o < colonnes; o++) {
                Button b = new Button();
                b.setLayoutX(cx);
                b.setLayoutY(cy);
                b.setPrefSize(base_x, base_y);
                b.setBorder(redBorder);
                b.setBackground(whiteBackground);
                b.setId("b");
                HashMap<Integer, Integer> cos = new HashMap<>();
                cos.put((i + 1), (o + 1));
                buttons.put(b, cos);
                pane.getChildren().add(b);
                cx = cx + base_x;
                ie++;
            }
            cx = 0;
            cy = cy + base_y;
        }
        pane.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, null, null)));
        pane.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.ENTER) {
                    System.out.println(event.getCode());
                    verify();
                }
                System.out.println(event.getCode());
            }
        });
        System.out.println(ie);
    }

    public void colorCases() {
        GameColors a = GameColors.BLUE;
        GameColors b = GameColors.GREEN;
        GameColors c = GameColors.RED;
        ArrayList<GameColors> gameColors = new ArrayList<>();
        gameColors.add(a);
        gameColors.add(b);
        gameColors.add(c);
        Random r = new Random();
        for (Button bu : buttons.keySet()) {
            int x = r.nextInt(3);
            bu.setBackground(new Background(new BackgroundFill(gameColors.get(x).getColor(), null, null)));
            bu.setId("" + gameColors.get(x).getId());

            bu.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if (!inE) {
                        inE = true;
                        iEiD = Integer.parseInt(bu.getId());
                        inEB = bu;
                        inEB.setText("1");
                        inEB.setStyle("-fx-text-color: white; -fx-alignment: center; -fx-font-size: 20px");
                    } else {
                        if (distance(inEB, bu) != 1) {
                            System.out.println(distance(inEB, bu));
                            return;
                        }
                        int xe = r.nextInt(3);
                        int id = Integer.parseInt(bu.getId());
                        int xc = Utils.addColors(id, iEiD);
                        GameColors gC = GameColors.getColor(xc);
                        Color nC = gC.getColor();
                        bu.setBackground(new Background(new BackgroundFill(nC, null, null)));
                        bu.setId("" + gC.getId());
                        inEB.setBackground(new Background(new BackgroundFill(gameColors.get(xe).getColor(), null, null)));
                        inEB.setId("" + gameColors.get(xe).getId());
                        inEB.setText("");
                        inE = false;
                        iEiD = -1;
                        inEB = null;
                        if (gC.equals(GameColors.PURPLE)) {
                            if (!LEAGUE_OF_LINALOL_ONE) {
                                JOptionPane.showMessageDialog(null, "Tu as créé du LeagueOfLinalol, enfin ... \n Un morceau ! Tu dois en aligner 3 et appuyer sur ENTRER !");
                                LEAGUE_OF_LINALOL_ONE = true;
                            }
                            verify();
                        }
                        if (gC.equals(GameColors.YELLOW)) {
                            if (!PIKACHUIDAL_ACID_ONE) {
                                JOptionPane.showMessageDialog(null, "Oh !! Mais ce n'est pas de l'Acide Pikachuïdal ! \n et bien ... non c'est un bout... ! aligne en 3 et appuis sur ENTRER ! \n Mais fais attention en le manipulant ... c'est corrosif !");
                                PIKACHUIDAL_ACID_ONE = true;
                            }
                            verify();
                        }
                        if (gC.equals(GameColors.BLEUROI)) {
                            if (!SPRITESAMERE_ANHYDRE_ONE) {
                                JOptionPane.showMessageDialog(null, "Du Sprite ... ah nan il manque de l'eau ! \n Ah ! Mais c'est du SpriteSaMere-Anhydre ! aligne en 3et appuis sur ENTRER !");
                                SPRITESAMERE_ANHYDRE_ONE = true;
                            }
                            verify();
                        }
                    }
                }
            });
        }
    }

    public void verify() {
        for (Button b : buttons.keySet()) {
            b.setDisable(true);
            if (b.getId().equals("40")) {
                if (!PIKACHUIDAL_ACID) {
                    if (isAlign(b, 1)) {
                        JOptionPane.showMessageDialog(null, "Oh ! Mais tu as synthétisé de l'acide pikachuïdal !");
                        PIKACHUIDAL_ACID = true;
                    }
                }
            } else if (b.getId().equals("520")) {
                if (!SPRITESAMERE_ANHYDRE) {
                    if (isAlign(b, 2)) {
                        JOptionPane.showMessageDialog(null, "Oh ! Mais tu as fait du SpriteSaMere-Anhydre !");
                        SPRITESAMERE_ANHYDRE = true;
                    }
                }
            } else if (b.getId().equals("320")) {
                if (!LEAGUE_OF_LINALOL) {
                    if (isAlign(b, 3)) {
                        JOptionPane.showMessageDialog(null, "Oh ! Mais tu as créé le LigueOfLinalol !");
                        LEAGUE_OF_LINALOL = true;
                    }
                }
            }
        }
        for (Button b : buttons.keySet()) {
            b.setDisable(false);
        }
        if (PIKACHUIDAL_ACID && LEAGUE_OF_LINALOL && SPRITESAMERE_ANHYDRE) {
            JOptionPane.showMessageDialog(null, "Tu as gagné :O tu peux maintenant appuyer sur Ok et faire échap -> échap pour revenir sur l'écran principal");
            Main.premièreEpreuve = true;
        }


    }

    public boolean isAlign(Button b, int i) {

        switch (i) {
            case 1:
                if (b.getId().equals("40")) {
                    return b(b);
                }
                break;
            case 2:
                if (b.getId().equals("520")) {
                    return b(b);
                }
                break;
            case 3:
                if (b.getId().equals("320")) {
                    return b(b);
                }
                break;
        }

        return false;
    }

    public boolean b(Button b) {
        int cosX = getCos(b)[0];
        int cosY = getCos(b)[1];
        int[] test01 = new int[2];
        int[] test02 = new int[2];
        int[] test03 = new int[2];
        int[] test04 = new int[2];
        int[] test05 = new int[2];
        int[] test06 = new int[2];
        int[] test07 = new int[2];
        int[] test08 = new int[2];
        int[] test09 = new int[2];
        int[] test10 = new int[2];
        int[] test11 = new int[2];
        int[] test12 = new int[2];
        /// X
        test01[0] = (cosX + 1);
        test01[1] = cosY;
        test02[0] = (cosX + 2);
        test02[1] = cosY;
        Button b01 = getButtonWithCos(test01);
        Button b02 = getButtonWithCos(test02);

        test03[0] = (cosX - 1);
        test03[1] = cosY;
        test04[0] = (cosX - 2);
        test04[1] = cosY;
        Button b03 = getButtonWithCos(test03);
        Button b04 = getButtonWithCos(test04);

        test05[0] = (cosX + 1);
        test05[1] = cosY;
        test06[0] = (cosX - 1);
        test06[1] = cosY;
        Button b05 = getButtonWithCos(test05);
        Button b06 = getButtonWithCos(test06);


        /// Y
        test07[0] = cosX;
        test07[1] = (cosY + 1);
        test08[0] = (cosX);
        test08[1] = (cosY + 2);
        Button b07 = getButtonWithCos(test07);
        Button b08 = getButtonWithCos(test08);

        test09[0] = cosX;
        test09[1] = (cosY + 1);
        test10[0] = (cosX);
        test10[1] = (cosY - 1);
        Button b09 = getButtonWithCos(test09);
        Button b10 = getButtonWithCos(test10);

        test11[0] = cosX;
        test11[1] = (cosY - 1);
        test12[0] = (cosX);
        test12[1] = (cosY - 2);
        Button b11 = getButtonWithCos(test11);
        Button b12 = getButtonWithCos(test12);
        if (b.getId() != null & b01.getId() != null && b02.getId() != null) {
            if (b.getId().equals(b01.getId())) {
                if (b.getId().equals(b02.getId())) {
                    return true;
                }
            }
        }
        if (b.getId() != null & b01.getId() != null && b02.getId() != null) {
            if (b.getId().equals(b03.getId())) {
                if (b.getId().equals(b04.getId())) {
                    return true;
                }
            }
        }
        if (b.getId() != null & b01.getId() != null && b02.getId() != null) {
            if (b.getId().equals(b05.getId())) {
                if (b.getId().equals(b06.getId())) {
                    return true;
                }
            }
        }
        if (b.getId() != null & b01.getId() != null && b02.getId() != null) {
            if (b.getId().equals(b07.getId())) {
                if (b.getId().equals(b08.getId())) {
                    return true;
                }
            }
        }
        if (b.getId() != null & b01.getId() != null && b02.getId() != null) {
            if (b.getId().equals(b09.getId())) {
                if (b.getId().equals(b10.getId())) {
                    return true;
                }
            }
        }
        if (b.getId() != null & b01.getId() != null && b02.getId() != null) {
            if (b.getId().equals(b11.getId())) {
                if (b.getId().equals(b12.getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    public int[] getCos(Button b) {
        int[] i = new int[2];
        HashMap<Integer, Integer> cosB = buttons.get(b);
        int cosX = 0;
        int cosY = 0;
        for (int x : cosB.keySet()) {
            cosX = x;
        }
        cosY = cosB.get(cosX);
        i[0] = cosX;
        i[1] = cosY;
        return i;
    }

    public Button getButtonWithCos(int[] cos) {
        int cosX = cos[0];
        int cosY = cos[1];
        HashMap<Integer, Integer> cosS = new HashMap<>();
        cosS.put(cosX, cosY);
        Map<HashMap<Integer, Integer>, Button> snottub = buttons.entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
        if (snottub.containsKey(cosS)) {
            return snottub.get(cosS);
        }
        return snottub.get(0);
    }

    public double distance(Button b01, Button b02) {

        int[] i01 = getCos(b01);
        int[] i02 = getCos(b02);

        int x1 = i01[0];
        int x2 = i02[0];

        int y1 = i01[1];
        int y2 = i02[1];
        System.out.println(x1 + " " + x2 + " " + y1 + " " + y2);
        double distance = Math.sqrt(((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2)));
        return distance;
    }

    static private String convertDecimalToFraction(double x) {
        if (x < 0) {
            return "-" + convertDecimalToFraction(-x);
        }
        double tolerance = 1.0E-6;
        double h1 = 1;
        double h2 = 0;
        double k1 = 0;
        double k2 = 1;
        double b = x;
        do {
            double a = Math.floor(b);
            double aux = h1;
            h1 = a * h1 + h2;
            h2 = aux;
            aux = k1;
            k1 = a * k1 + k2;
            k2 = aux;
            b = 1 / (b - a);
        } while (Math.abs(x - h1 / k1) > x * tolerance);

        return h1 + "/" + k1;
    }
}
